package com.chh.payer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.chh.payer.bean.Payer;

@RepositoryRestResource (collectionResourceRel ="payer", path="payer")
public interface PayerRepository extends JpaRepository<Payer, Long>{
	
	

}
