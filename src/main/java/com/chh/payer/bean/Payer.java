package com.chh.payer.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
public class Payer {

	@Id @Setter (AccessLevel.PROTECTED)
	@GeneratedValue (strategy = GenerationType.AUTO)
	public long payerId;
	public String payerName;
	public String feeSchedulePayerName;
	public String feeSchedulePayerId;
	public int medGroupCode;
	public int payerClassTypeCode;
	public String payerWebsiteText;
	
	
	/*
	public String XP_CLAIM_TYPE;
	public int MDCR_CARRIER_FLG;
	public int ELCTR_ELGBL_FLG;
	*/

}
