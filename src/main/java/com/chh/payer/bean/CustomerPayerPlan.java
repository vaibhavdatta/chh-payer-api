package com.chh.payer.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @EqualsAndHashCode @ToString
public class CustomerPayerPlan {
	
	@Id @Setter (AccessLevel.PROTECTED)
	@GeneratedValue (strategy = GenerationType.AUTO)
	public long customerPayerPlanId;
	public long customerId;
	public long planId;
	public long policyHolderId;
	public long eligibilityId;
	public long planTypeId;
	public String verificationType;
	public String verificationStatus;
	public String verificationMethod;
	public long policyNumber;
	public String policyGroupName;
	public String policyGroupText;
	
	/*
	POLICY_NUM
	POLICY_GROUP_NAM
	POLICY_GROUP_TXT
	PAYER_RLT_TYPE_CDE
	RLT_PAYER_NAM
	PLAN_YR_CDE
	PLAN_YR_START_DTE
	MDCR_ID_TXT
	EFFECT_START_DTE
	EFFECT_END_DTE
	TRMNT_DTE
	OUT_OF_PKT_DLR
	DEDUCTIBLE_DLR
	CO_PAY_DLR
	MAX_ANL_BNFT_DLR
	HRA_DLR
	INSR_COVERED_PCT
	INSR_COVERED_AFTER_OOP_PCT
	POLICY_HOLDER_RLT_TYPE_CDE
	SORT_ORDER_CDE
	GNDR_ID
	APS_RERUN_FLG
	INSR_COVER_PRIM_DEDCT_FLG
	APPLY_PRIM_DEDCT_TO_DEDCT_FLG
	DEDUCTIBLE_APPLY_TO_OOP_FLG
	MED_GROUP_AT_RISK_FLG
	MED_GROUP_CHG_TRK_TXT
	*/

}
