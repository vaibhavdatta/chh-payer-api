package com.chh.payer.bean;

import java.util.Date;

import javax.persistence.Entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @ToString @EqualsAndHashCode
public class PayerPlan {
	
	public long planId;
	public long payerId;
	public String groupId;
	public String planCode;
	public String planName;
	public String rcmPlanId;
	public int userSelectableFlag;
	public int allowContinueCareOrderFlag;
	public String planInternalContactName;
	public String contractName;
	public Date ContractLifeCycleStartDate;
	
	/*
	CNTRCT_LIFE_CYCLE_START_DTE
	CNTRCT_LFCYCL_END_DTE
	CNTRCT_LFCYCL_CMNT_TXT
	MDCD_NUM
	RENT_PRCDR_CDE
	SALE_PRCDR_CDE
	SPECIAL_PROG_ID
	AGE_RLVNT_ID
	NHXS_PLAN_NAM
	FEE_SCHED_ID
	LGTH_ALW_FOR_POLICY_NUM
	BILL_NUM_RESP
	NONCOVERED_DISC_PCT
	INSR_PROD_NAM
	FUND_SRC_CDE
	CNTRCT_SUB_GROUP_CDE
	TENANT_CDE
	BILL_FREQ_TYPE_CDE
	MSG01_MAP_ID
	EDI_INSR_TYPE_CDE
	RFND_MTHD_CDE
	INSR_TYPE_CDE
	PLAN_TYPE_CDE
	ACTIVE_FLG
	SIGNTR_LINE_TXT
	PREFER_SHPR_TXT
	ICD_VRSN_TXT
	PA_SUBMISSION_URL_TXT
	CLAIM_FILING_ID_TXT
	*/


}
